//
//  SelectSchoolViewController.swift
//  Named App
//
//  Created by Prathyusha Pillari on 7/3/18.
//  Copyright © 2018 Prathyusha Pillari. All rights reserved.
//

import UIKit

class SelectSchoolViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var arrayOfSchools = ["Redmond High School", "Tesla High School", "Woodenville High School"]
    
    // number of scroll thingys
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // number of schools in the scroll thingy
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return arrayOfSchools.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayOfSchools[row]
    }
    
    @IBOutlet weak var selectSchoolPickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectSchoolPickerView.delegate = self
        self.selectSchoolPickerView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
