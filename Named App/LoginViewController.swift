//
//  LoginViewController.swift
//  Named App
//
//  Created by Prathyusha Pillari on 7/3/18.
//  Copyright © 2018 Prathyusha Pillari. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    static var usernamePasswordDictionary:[String:String] = ["12":"honey", "17":"sona"]

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        let userName = userNameTextField.text;
        let userPassword = userPasswordTextField.text;
        
        // Check for empty fields
        if ((userName?.isEmpty)! || (userPassword?.isEmpty)!){
            // Display alert message
            displayMyAlertMessage(message: "All fields are required");
            return;
        }
        
        if(LoginViewController.usernamePasswordDictionary.keys.contains(userPassword!) == false || LoginViewController.usernamePasswordDictionary.values.contains(userName!) == false){
            // Display alert message
            displayMyAlertMessage(message: "Incorrect username or password");
            return;
        }
    }
    
    func displayMyAlertMessage(message: String){
        let myAlert = UIAlertController(title:"Alert", message: message, preferredStyle:  UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"OK", style:UIAlertActionStyle.default, handler:nil);
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion:nil);
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
