//
//  SignUpPageViewController.swift
//  Named App
//
//  Created by Prathyusha Pillari on 7/3/18.
//  Copyright © 2018 Prathyusha Pillari. All rights reserved.
//

import UIKit

class SignUpPageViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUp(_ sender: UIButton) {
        let userName = username.text;
        let userEmail = email.text;
        let userPassword = password.text;
        let confirmPassword = self.confirmPassword.text;
        
        // Check for empty fields
        if ((userName?.isEmpty)! || (userEmail?.isEmpty)! || (userPassword?.isEmpty)! || (confirmPassword?.isEmpty)!){
            // Display alert message
            displayMyAlertMessage(message: "All fields are required");
            return;
        }
        
        // Check if username already exists
        if (LoginViewController.usernamePasswordDictionary.values.contains(userName!) == true){ //if(object.usernamePasswordDictionary.){
            // Display an alert message
            displayMyAlertMessage(message: "Username already exists");
            return;
        }
        
        // Check if passwords match
        if(userPassword != confirmPassword){
            // Display an alert message
            displayMyAlertMessage(message: "Passwords do not match");
            return;
        }
        
        if(isValidEmail(testStr: userEmail!) == false){
            // Display an alert message
            displayMyAlertMessage(message: "Enter valid email");
            return;
        }
        
        if((userPassword?.count)! < 8){
            // Display an alert message
            displayMyAlertMessage(message: "Password needs to be atleast 8 charecters");
            return;
        }
        
        // Store data
        LoginViewController.usernamePasswordDictionary.updateValue(userName!, forKey: userPassword!)
        
        // Display alert message with confirmation
        let myAlert = UIAlertController(title:"Alert", message: "You are signed up!", preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title: "OK", style:UIAlertActionStyle.default){ action in
                    self.dismiss(animated: true, completion:nil);
        }
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion:nil);
    }

    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }

    func displayMyAlertMessage(message: String){
        let myAlert = UIAlertController(title:"Alert", message: message, preferredStyle:  UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"OK", style:UIAlertActionStyle.default, handler:nil);

        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion:nil);

    }
}





